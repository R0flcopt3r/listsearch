# listsearch

This is a script that lets you search for a file or folder in the working directory.
It is basically a alias for ls -hAl | grep -i. I made it as a learning exercise for
getopts.

### Usage:

lsr [options] [filename]    Example: lsr -v Documents

### Options:
-v      Invert the sense of matching, to select non-matching lines.
-h      Output this message and exits

#### Dependencies
grep
ls

